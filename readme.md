## Installation

### Prerequisites
* git is installed
* Node.js 0.10.x and npm are installed

### Prepare dtServer
Currently the server needs a flag to be set to open the http port:
This is done by inserting `-Dcom.dynatrace.diagnostics.startRxAdapter=true` into `dtserver.ini`


### Checkout and module installation
```sh
    $ git clone https://derpunkt@bitbucket.org/derpunkt/nodeloadtest.git
    $ cd nodeloadtest
    $ npm install
```

### Agent setup
```sh
    $ cd local
    $ cp agent.js.local agent.js

    # Edit agent path, name and server address (if required)
    $ nano | vi | whatever agent.js
```

### Configuration
Edit config.js to set the URI to the fibonacci backend

## Running the app

### Express.js

```sh
    $ NODE_ENV=<production | development> PORT=<port> bin/www
```
* The app should be accessible at http://<server_address>:<port> and show a simple website.
* For fibonacci test scenario against a backend service go to http://<server_address>:<port>/fib

