var express = require('express');
var router = express.Router();
var sa = require('superagent');
var config = require('../config');

/* GET home page. */
router.get('/', function (req, res, next) {


    res.render('index', {title: 'Express'});
});

router.get('/fib/:par', function (req, res, next) {

    sa.get(config.fibUri + '/' + req.params.par).end(function (e, r) {
        if (e) return next(e);

        res.render('index', {
            title: "Fibonacci Result",
            fib: r.body.result
        });

    });
});

router.get('/fibstub', function (req, res, next) {

    res.json({"result": "12"});

});

module.exports = router;
